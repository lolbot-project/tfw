from quart import Quart, make_response
from aiohttp import ClientSession
import asyncio
import json

app = Quart(__name__)
app.session = ClientSession(loop=asyncio.get_event_loop())
app.cfg = json.load(open('config.json'))


def construct(domain, key):
    base = 'https://www.whoisxmlapi.com/whoisserver/WhoisService'
    base += f'?apiKey={key}&outputFormat=JSON&domainName={domain}'
    return base


@app.route('/api/whois/<domain>')
async def whois(domain):
    async with app.session.get(construct(domain, app.cfg['key'])) as yes:
        a = await yes.text()
        res = await make_response(a)
        res.mimetype = 'application/json'
        return res
